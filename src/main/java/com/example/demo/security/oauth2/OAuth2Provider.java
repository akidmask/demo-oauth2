package com.example.demo.security.oauth2;

public enum OAuth2Provider {

    LOCAL, GITHUB
}
