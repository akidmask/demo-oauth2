# Demo Oauth2

**Creating Github App**

Github apps can be created from https://github.com/settings/apps


**Start MySQL Container**

`docker-compose up -d`


**Running Demo App**

`export GITHUB_CLIENT_ID=...&&export GITHUB_CLIENT_SECRET=...&&./mvnw clean spring-boot:run`


**Testing Demo App**

Hit the below URL from browser:
`http://localhost:8080/oauth2/authorization/github`

It will be redirected to the Github OAuth2 provider login form and asked for user permission. If user allows permissions then OAuth2 provider will redirect the user to the configured callback URL.


**References**
- https://docs.spring.io/spring-security/site/docs/5.2.12.RELEASE/reference/html/oauth2.html#oauth2login-sample-redirect-uri
- https://spring.io/guides/tutorials/spring-boot-oauth2/
- https://github.com/ivangfr/springboot-react-social-login



